﻿using BL.Interfaces;
using BL.Models;
using DAL.Base;
using DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace BL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork unitOFWork;
        public IUnitOfWork UnitOFWork => unitOFWork;

        public ProjectService(IUnitOfWork unitOFWork)
        {
            this.unitOFWork = unitOFWork;
        }

        public IEnumerable<Project> GetAll()
        {
            return unitOFWork.ProjectRepository.GetAll();
        }

        public Project GetById(int id)
        {
            return unitOFWork.ProjectRepository.GetById(id);
        }

        public void Add(IEnumerable<Project> entities)
        {
            unitOFWork.ProjectRepository.Add(entities);
            unitOFWork.SaveChanges();
        }

        public void Add(Project entity)
        {
            unitOFWork.ProjectRepository.Add(entity);
            unitOFWork.SaveChanges();
        }

        public void Remove(IEnumerable<Project> entities)
        {
            unitOFWork.ProjectRepository.Remove(entities);
            unitOFWork.SaveChanges();
        }

        public void Remove(Project entity)
        {
            unitOFWork.ProjectRepository.Remove(entity);
            unitOFWork.SaveChanges();
        }

        public void Update(IEnumerable<Project> entities)
        {
            unitOFWork.ProjectRepository.Update(entities);
            unitOFWork.SaveChanges();
        }

        public void Update(Project entity)
        {
            unitOFWork.ProjectRepository.Update(entity);
            unitOFWork.SaveChanges();
        }

        public IEnumerable<ProjectInfo> GetProjectWithTasks()
        {
            var projects = unitOFWork.ProjectRepository.GetAll();

            var result = projects
                .Join(unitOFWork.TaskRepository.GetAll(), p => p.Id, t => t.ProjectId, (p, t) => new { p, t })
                .Join(unitOFWork.UserRepository.GetAll(), e => e.p.TeamId, u => u.TeamId, (e, u) => new { Project = e.p, Task = e.t, User = u })
                .GroupBy(e => e.Project)
                .Select(e => new ProjectInfo
                {
                    Project = e.Key,
                    LongestTask = e.Select(x => x.Task).Where(x => x.Description.Length == e.Select(x => x.Task.Description.Length).Max()).FirstOrDefault(),
                    ShortestTask = e.Select(x => x.Task).Where(x => x.Name.Length == e.Select(x => x.Task.Name.Length).Min()).FirstOrDefault(),
                });

            return result;
        }
    }
}
