﻿using BL.Interfaces;
using BL.Models;
using DAL.Base;
using DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace BL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork unitOFWork;
        public IUnitOfWork UnitOFWork => unitOFWork;

        public UserService(IUnitOfWork unitOFWork)
        {
            this.unitOFWork = unitOFWork;
        }

        public IEnumerable<User> GetAll()
        {
            return unitOFWork.UserRepository.GetAll();
        }

        public User GetById(int id)
        {
            return unitOFWork.UserRepository.GetById(id);
        }

        public void Add(IEnumerable<User> entities)
        {
            unitOFWork.UserRepository.Add(entities);
        }

        public void Add(User entity)
        {
            unitOFWork.UserRepository.Add(entity);
        }

        public void Remove(IEnumerable<User> entities)
        {
            unitOFWork.UserRepository.Remove(entities);
        }

        public void Remove(User entity)
        {
            unitOFWork.UserRepository.Remove(entity);
        }

        public void Update(IEnumerable<User> entities)
        {
            unitOFWork.UserRepository.Update(entities);
        }

        public void Update(User entity)
        {
            unitOFWork.UserRepository.Update(entity);
        }

        public IEnumerable<UserTasks> GetOrderedUsersWithOrderedTasks()
        {
            var users = unitOFWork.UserRepository.GetAll();

            var result = users
                .Join(unitOFWork.TaskRepository.GetAll(), u => u.Id, t => t.PerformerId, (u, t) => new { User = u, Tasks = t })
                .GroupBy(e => e.User)
                .Select(e => new UserTasks { User = e.Key, Tasks = e.Select(t => t.Tasks).OrderByDescending(t => t.Name.Length).AsEnumerable() })
                .OrderBy(e => e.User.FirstName)
                .AsEnumerable();

            return result;
        }

        public UserInfo GetUserInfo(int userId)
        {
            var projects = unitOFWork.ProjectRepository.GetAll();
            var tasks = unitOFWork.TaskRepository.GetAll();

            var result = projects
                .Where(e => e.AuthorId == userId && e.CreatedAt == projects.Max(e => e.CreatedAt))
                .Take(1)
                .Join(unitOFWork.TaskRepository.GetAll(), p => p.Id, t => t.ProjectId, (p, t) => new { p, t })
                .GroupBy(e => e.p)
                .Select(e => new
                {
                    LastProject = e.Key,
                    NumOfTasks = e.Count()
                })
                .FirstOrDefault();

            var result2 = tasks
                .Where(e => e.PerformerId == userId)
                .GroupBy(e => e.PerformerId)
                .Select(e => new
                {
                    Incomplete = e.Where(x => x.State == TaskStates.Canceled || x.State == TaskStates.Started).Count(),
                    Longest = e.Where(x => x.FinishedAt - x.CreatedAt == e.Select(x => x.FinishedAt - x.CreatedAt).Max()).FirstOrDefault()
                })
                .FirstOrDefault();

            return new UserInfo
            {
                User = unitOFWork.UserRepository.GetById(userId),
                OverallNumOfCanceledTasks = result2?.Incomplete,
                LongestTask = result2?.Longest,
                LastProject = result?.LastProject,
                OverallNumOfTasks = result?.NumOfTasks
            };
        }
    }
}
