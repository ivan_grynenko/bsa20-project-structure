﻿using BL.Interfaces;
using DAL.Base;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork unitOFWork;
        public IUnitOfWork UnitOFWork => unitOFWork;

        public TaskService(IUnitOfWork unitOFWork)
        {
            this.unitOFWork = unitOFWork;
        }

        public IEnumerable<Tasks> GetAll()
        {
            return unitOFWork.TaskRepository.GetAll();
        }

        public Tasks GetById(int id)
        {
            return unitOFWork.TaskRepository.GetById(id);
        }

        public void Add(IEnumerable<Tasks> entities)
        {
            unitOFWork.TaskRepository.Add(entities);
        }

        public void Add(Tasks entity)
        {
            unitOFWork.TaskRepository.Add(entity);
        }

        public void Remove(IEnumerable<Tasks> entities)
        {
            unitOFWork.TaskRepository.Remove(entities);
        }

        public void Remove(Tasks entity)
        {
            unitOFWork.TaskRepository.Remove(entity);
        }

        public void Update(IEnumerable<Tasks> entities)
        {
            unitOFWork.TaskRepository.Update(entities);
        }

        public void Update(Tasks entity)
        {
            unitOFWork.TaskRepository.Update(entity);
        }

        public IDictionary<string, int> GetNumberOfTasksByUser(int userId)
        {
            var result = unitOFWork.TaskRepository.GetAll()
                .Where(e => e.PerformerId == userId)
                .GroupBy(e => e.ProjectId)
                .Select(e => new { ProjectId = e.Key, Count = e.Count() })
                .Join(unitOFWork.ProjectRepository.GetAll(), t => t.ProjectId, p => p.Id, (t, p) => new { p.Name, t.Count })
                .ToDictionary(d => d.Name, d => d.Count);

            return result;
        }

        public IEnumerable<Tasks> GetTasksByUserWithMaxNameLength(int userId, int maxLength)
        {
            var result = unitOFWork.TaskRepository.GetAll()
                .Where(e => e.PerformerId == userId && e.Name.Length < maxLength);

            return result;
        }

        public IEnumerable<Tasks> GetTasksByUserInCurrentYear(int userId)
        {
            var result = unitOFWork.TaskRepository.GetAll()
                .Where(e => e.PerformerId == userId && e.FinishedAt.Year == DateTime.Now.Year);

            return result;
        }
    }
}
