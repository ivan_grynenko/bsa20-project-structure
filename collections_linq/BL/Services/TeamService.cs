﻿using BL.Interfaces;
using DAL.Base;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IUnitOfWork unitOFWork;
        public IUnitOfWork UnitOFWork => unitOFWork;

        public TeamService(IUnitOfWork unitOFWork)
        {
            this.unitOFWork = unitOFWork;
        }

        public IEnumerable<Team> GetAll()
        {
            return unitOFWork.TeamRepository.GetAll();
        }

        public Team GetById(int id)
        {
            return unitOFWork.TeamRepository.GetById(id);
        }

        public void Add(IEnumerable<Team> entities)
        {
            unitOFWork.TeamRepository.Add(entities);
        }

        public void Add(Team entity)
        {
            unitOFWork.TeamRepository.Add(entity);
        }

        public void Remove(IEnumerable<Team> entities)
        {
            unitOFWork.TeamRepository.Remove(entities);
        }

        public void Remove(Team entity)
        {
            unitOFWork.TeamRepository.Remove(entity);
        }

        public void Update(IEnumerable<Team> entities)
        {
            unitOFWork.TeamRepository.Update(entities);
        }

        public void Update(Team entity)
        {
            unitOFWork.TeamRepository.Update(entity);
        }

        public IEnumerable<(int? Id, string Name, IEnumerable<User> Users)> GetTeamsOfCertainAge(int minAge)
        {
            var users = unitOFWork.UserRepository.GetAll();

            var result = users
                .GroupBy(e => e.TeamId)
                .Where(g => g.All(e => e.Birthday.AddYears(minAge) <= DateTime.Now) && g.Key != null)
                .Join(unitOFWork.TeamRepository.GetAll(), u => u.Key, t => t.Id, (u, t) => new { Id = u.Key, t.Name, Users = u.OrderByDescending(e => e.RegisteredAt).AsEnumerable() })
                .Select(e => (e.Id, e.Name, e.Users));

            return result;
        }
    }
}
