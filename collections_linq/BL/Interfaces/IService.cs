﻿using System;
using System.Collections.Generic;

namespace BL.Interfaces
{
    public interface IService<TEntity>
    {
        IEnumerable<TEntity> GetAll();
        TEntity GetById(int id);
        void Add(IEnumerable<TEntity> entities);
        void Add(TEntity entity);
        void Remove(IEnumerable<TEntity> entities);
        void Remove(TEntity entity);
        void Update(IEnumerable<TEntity> entities);
        void Update(TEntity entity);
    }
}
