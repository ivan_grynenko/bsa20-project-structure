﻿using DAL.Base;
using DAL.Models;
using System.Collections.Generic;

namespace BL.Interfaces
{
    public interface ITeamService : IService<Team>
    {
        IUnitOfWork UnitOFWork { get; }

        IEnumerable<(int? Id, string Name, IEnumerable<User> Users)> GetTeamsOfCertainAge(int minAge);
    }
}