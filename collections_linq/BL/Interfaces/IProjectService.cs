﻿using BL.Models;
using DAL.Base;
using DAL.Models;
using System.Collections.Generic;

namespace BL.Interfaces
{
    public interface IProjectService : IService<Project>
    {
        IUnitOfWork UnitOFWork { get; }

        IEnumerable<ProjectInfo> GetProjectWithTasks();
    }
}