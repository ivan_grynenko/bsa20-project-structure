﻿using DAL.Base;
using DAL.Models;
using System.Collections.Generic;

namespace BL.Interfaces
{
    public interface ITaskService : IService<Tasks>
    {
        IUnitOfWork UnitOFWork { get; }

        IDictionary<string, int> GetNumberOfTasksByUser(int userId);
        IEnumerable<Tasks> GetTasksByUserInCurrentYear(int userId);
        IEnumerable<Tasks> GetTasksByUserWithMaxNameLength(int userId, int maxLength);
    }
}