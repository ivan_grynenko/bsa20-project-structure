﻿using BL.Models;
using DAL.Base;
using DAL.Models;
using System.Collections.Generic;

namespace BL.Interfaces
{
    public interface IUserService : IService<User>
    {
        IUnitOfWork UnitOFWork { get; }

        IEnumerable<UserTasks> GetOrderedUsersWithOrderedTasks();
        UserInfo GetUserInfo(int userId);
    }
}