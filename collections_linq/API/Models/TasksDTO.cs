﻿using DAL.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    public class TasksDTO
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime FinishedAt { get; set; }
        [Required]
        [EnumDataType(typeof(TaskStates))]
        public TaskStates State { get; set; }
        [Required]
        public int ProjectId { get; set; }
        [Required]
        public int PerformerId { get; set; }
    }
}
