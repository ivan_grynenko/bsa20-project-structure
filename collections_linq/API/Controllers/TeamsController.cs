﻿using System;
using System.Collections.Generic;
using System.Linq;
using API.Models;
using AutoMapper;
using BL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService teamService;
        private readonly IMapper mapper;

        public TeamsController(ITeamService teamService, IMapper mapper)
        {
            this.teamService = teamService;
            this.mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> GetTeams()
        {
            return Ok(mapper.Map<IEnumerable<TeamDTO>>(teamService.GetAll()));
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDTO> GetTeamById(int id)
        {
            if (id < 1)
                return BadRequest();

            var team = teamService.GetById(id);

            if (team == null)
                return NotFound();
            else
                return Ok(mapper.Map<TeamDTO>(team));
        }

        [HttpGet("MinAge/{age}")]
        public ActionResult<IEnumerable<object>> GetTeamsOfCertainAge(int age)
        {
            if (age < 1)
                return BadRequest();

            return Ok(teamService.GetTeamsOfCertainAge(age).Select(e => new { e.Id, e.Name, Users = mapper.Map<IEnumerable<UserDTO>>(e.Users) }));
        }

        [HttpPost]
        public IActionResult AddTeams([FromBody] IEnumerable<TeamDTO> newTeams)
        {
            if (newTeams.Any(e => !TryValidateModel(e)))
                return BadRequest();

            var check = teamService.GetAll()
                .Select(e => e.Id)
                .Where(e => newTeams.Select(t => t.Id).Contains(e));

            if (check.Count() > 0)
                return BadRequest("One of the ids exists");

            teamService.Add(mapper.Map<IEnumerable<Team>>(newTeams));

            return NoContent();
        }

        [HttpPost]
        public IActionResult AddTeam([FromBody] TeamDTO newTeam)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (teamService.GetAll().ToList().Find(e => e.Id == newTeam.Id) != null)
                return BadRequest("Such id exists");

            teamService.Add(mapper.Map<Team>(newTeam));

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Remove(int id)
        {
            if (id < 1)
                return BadRequest();

            var item = teamService.GetById(id);

            if (item == null)
                return NotFound();

            try
            {
                teamService.Remove(item);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }

        [HttpPatch]
        public IActionResult UpdateTeams([FromBody] IEnumerable<TeamDTO> newTeams)
        {
            try
            {
                teamService.Update(mapper.Map<IEnumerable<Team>>(newTeams));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }

        [HttpPatch]
        public IActionResult UpdateTeam([FromBody] TeamDTO newTeam)
        {
            try
            {
                teamService.Update(mapper.Map<Team>(newTeam));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }
    }
}
