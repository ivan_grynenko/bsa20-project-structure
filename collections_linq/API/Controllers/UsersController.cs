﻿using System;
using System.Collections.Generic;
using System.Linq;
using API.Models;
using AutoMapper;
using BL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public UsersController(IUserService userService, IMapper mapper)
        {
            this.userService = userService;
            this.mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> GetUsers()
        {
            return Ok(mapper.Map<IEnumerable<UserDTO>>(userService.GetAll()));
        }

        [HttpGet("{id}")]
        public ActionResult<UserDTO> GetUserById(int id)
        {
            if (id < 1)
                return BadRequest();

            var user = userService.GetById(id);

            if (user == null)
                return NotFound();
            else
                return Ok(mapper.Map<UserDTO>(user));
        }

        [HttpGet("OrderWithTasks")]
        public ActionResult<IEnumerable<UserTasksDTO>> GetOrderedUsersWithOrderedTasks()
        {
            return Ok(mapper.Map<IEnumerable<UserTasksDTO>>(userService.GetOrderedUsersWithOrderedTasks()));
        }

        [HttpGet("{id}/UserInfo")]
        public ActionResult<UserInfoDTO> GetUserInfo(int id)
        {
            if (id < 1)
                return BadRequest();

            var user = userService.GetById(id);

            if (user == null)
                return NotFound();

            return Ok(mapper.Map<UserInfoDTO>(userService.GetUserInfo(id)));
        }

        [HttpPost]
        public IActionResult AddUsers([FromBody] IEnumerable<ProjectDTO> newUsers)
        {
            if (newUsers.Any(e => !TryValidateModel(e)))
                return BadRequest();

            var check = userService.GetAll()
                .Select(e => e.Id)
                .Where(e => newUsers.Select(t => t.Id).Contains(e));

            if (check.Count() > 0)
                return BadRequest("One of the ids exists");

            userService.Add(mapper.Map<IEnumerable<User>>(newUsers));

            return NoContent();
        }

        [HttpPost]
        public IActionResult AddUser([FromBody] ProjectDTO newUser)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (userService.GetAll().ToList().Find(e => e.Id == newUser.Id) != null)
                return BadRequest("Such id exists");

            userService.Add(mapper.Map<User>(newUser));

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Remove(int id)
        {
            if (id < 1)
                return BadRequest();

            var item = userService.GetById(id);

            if (item == null)
                return NotFound();

            try
            {
                userService.Remove(item);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }

        [HttpPatch]
        public IActionResult UpdateUsers([FromBody] IEnumerable<UserDTO> newUsers)
        {
            try
            {
                userService.Update(mapper.Map<IEnumerable<User>>(newUsers));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }

        [HttpPatch]
        public IActionResult UpdateUser([FromBody] UserDTO newUser)
        {
            try
            {
                userService.Update(mapper.Map<User>(newUser));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }
    }
}
