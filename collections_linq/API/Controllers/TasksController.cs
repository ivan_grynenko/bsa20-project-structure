﻿using System;
using System.Collections.Generic;
using System.Linq;
using API.Models;
using AutoMapper;
using BL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService taskService;
        private readonly IMapper mapper;

        public TasksController(ITaskService taskService, IMapper mapper)
        {
            this.taskService = taskService;
            this.mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TasksDTO>> GetTasks()
        {
            return Ok(mapper.Map<IEnumerable<TasksDTO>>(taskService.GetAll()));
        }

        [HttpGet("{id}")]
        public ActionResult<TasksDTO> GetTaskById(int id)
        {
            if (id < 1)
                return BadRequest();

            var task = taskService.GetById(id);

            if (task == null)
                return NotFound();
            else
                return Ok(mapper.Map<TasksDTO>(task));
        }

        [HttpGet("{id}/ByUser")]
        public ActionResult<Dictionary<string, int>> GetNumberOfTasksByUser(int id)
        {
            if (id < 1)
                return BadRequest();

            var task = taskService.GetById(id);

            if (task == null)
                return NotFound();

            return Ok(taskService.GetNumberOfTasksByUser(id));
        }

        [HttpGet("{id}/WithMaxLength/{maxLength}")]
        public ActionResult<IEnumerable<TasksDTO>> GetTasksByUserWithMaxNameLength(int id, int maxLength)
        {
            if (id < 1 || maxLength < 0)
                return BadRequest();

            var task = taskService.GetById(id);

            if (task == null)
                return NotFound();

            return Ok(mapper.Map<TasksDTO>(taskService.GetTasksByUserWithMaxNameLength(id, maxLength)));
        }

        [HttpGet("{id}/CurrentYear")]
        public ActionResult<IEnumerable<TasksDTO>> GetTasksByUserInCurrentYear(int id)
        {
            if (id < 1)
                return BadRequest();

            var task = taskService.GetById(id);

            if (task == null)
                return NotFound();

            return Ok(mapper.Map<TasksDTO>(taskService.GetTasksByUserInCurrentYear(id)));
        }

        [HttpPost]
        public IActionResult AddTasks([FromBody] IEnumerable<TasksDTO> newTasks)
        {
            if (newTasks.Any(e => !TryValidateModel(e)))
                return BadRequest();

            var check = taskService.GetAll()
                .Select(e => e.Id)
                .Where(e => newTasks.Select(t => t.Id).Contains(e));

            if (check.Count() > 0)
                return BadRequest("One of the ids exists");

            taskService.Add(mapper.Map<IEnumerable<Tasks>>(newTasks));

            return NoContent();
        }

        [HttpPost]
        public IActionResult AddTask([FromBody] TasksDTO newTask)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (taskService.GetAll().ToList().Find(e => e.Id == newTask.Id) != null)
                return BadRequest("Such id exists");

            taskService.Add(mapper.Map<Tasks>(newTask));

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult RemoveTask(int id)
        {
            if (id < 1)
                return BadRequest();

            var item = taskService.GetById(id);

            if (item == null)
                return NotFound();

            try
            {
                taskService.Remove(item);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }

        [HttpPatch]
        public IActionResult UpdateTasks([FromBody] IEnumerable<TasksDTO> newTasks)
        {
            try
            {
                taskService.Update(mapper.Map<IEnumerable<Tasks>>(newTasks));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }

        [HttpPatch]
        public IActionResult UpdateProject([FromBody] TasksDTO newTask)
        {
            try
            {
                taskService.Update(mapper.Map<Tasks>(newTask));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }
    }
}
