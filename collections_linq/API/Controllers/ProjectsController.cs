﻿using System;
using System.Collections.Generic;
using System.Linq;
using API.Models;
using AutoMapper;
using BL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService projectService;
        private readonly IMapper mapper;

        public ProjectsController(IProjectService projectService, IMapper mapper)
        {
            this.projectService = projectService;
            this.mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> GetProjects()
        {
            return Ok(mapper.Map<IEnumerable<ProjectDTO>>(projectService.GetAll()));
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> GetProjectById(int id)
        {
            if (id < 1)
                return BadRequest();

            var project = projectService.GetById(id);

            if (project == null)
                return NotFound();
            else
                return Ok(mapper.Map<ProjectDTO>(project));
        }

        [HttpGet("withtasks")]
        public ActionResult<IEnumerable<ProjectInfoDTO>> GetProjectWithTasks()
        {
            return Ok(mapper.Map<ProjectInfoDTO>(projectService.GetProjectWithTasks()));
        }

        [HttpPost]
        public IActionResult AddProjects([FromBody]IEnumerable<ProjectDTO> newProjects)
        {
            if (newProjects.Any(e => !TryValidateModel(e)))
                return BadRequest();

            var check = projectService.GetAll()
                .Select(e => e.Id)
                .Where(e => newProjects.Select(t => t.Id).Contains(e));

            if (check.Count() > 0)
                return BadRequest("One of the ids exists");

            projectService.Add(mapper.Map<IEnumerable<Project>>(newProjects));

            return NoContent();
        }

        [HttpPost]
        public IActionResult AddProject([FromBody] ProjectDTO newProject)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (projectService.GetAll().ToList().Find(e => e.Id == newProject.Id) != null)
                return BadRequest("Such id exists");

            projectService.Add(mapper.Map<Project>(newProject));

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Remove(int id)
        {
            if (id < 1)
                return BadRequest();

            var item = projectService.GetById(id);

            if (item == null)
                return NotFound();

            try
            {
                projectService.Remove(item);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }

        [HttpPatch]
        public IActionResult UpdateProjects([FromBody] IEnumerable<ProjectDTO> newProjects)
        {
            try
            {
                projectService.Update(mapper.Map<IEnumerable<Project>>(newProjects));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }

        [HttpPatch]
        public IActionResult UpdateProject([FromBody] ProjectDTO newProject)
        {
            try
            {
                projectService.Update(mapper.Map<Project>(newProject));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }
    }
}
