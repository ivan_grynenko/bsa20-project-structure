﻿using System;

namespace DAL.Models
{
    public class Team : BaseModel
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
