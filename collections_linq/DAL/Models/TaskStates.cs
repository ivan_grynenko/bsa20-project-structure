﻿using System;

namespace DAL.Models
{
    public enum TaskStates
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
