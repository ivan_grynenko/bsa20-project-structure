﻿using Bogus;
using Bogus.Extensions;
using DAL.Base;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Linq;

namespace DAL.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository()
        {
            FakeData();
        }

        private void FakeData()
        {
            var id = 1;
            var fake = new Faker<User>()
                .RuleFor(u => u.Id, f => id++)
                .RuleFor(u => u.FirstName, (f, u) => f.Name.FirstName())
                .RuleFor(u => u.LastName, (f, u) => f.Name.LastName())
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.FirstName, u.LastName))
                .RuleFor(u => u.Birthday, (f, u) => f.Date.Between(new DateTime(1960, 1, 1), new DateTime(2010, 1, 1)))
                .RuleFor(u => u.RegisteredAt, (f, u) => f.Date.Between(new DateTime(2010, 1, 1), DateTime.Now))
                .RuleFor(u => u.TeamId, f => f.Random.Int(1, 5).OrNull(f));

            Entities = Enumerable.Range(1, 30)
                .Select(e => fake.Generate());
        }
    }
}
