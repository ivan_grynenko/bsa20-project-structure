﻿using Bogus;
using DAL.Base;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Linq;

namespace DAL.Repositories
{
    public class ProjectRepository : Repository<Project>, IProjectRepository
    {
        public ProjectRepository()
        {
            var random = new Random();
            var id = 1;
            var fake = new Faker<Project>()
                .RuleFor(p => p.Id, f => id++)
                .RuleFor(p => p.Name, f => f.Random.Word())
                .RuleFor(p => p.TeamId, f => f.Random.Int(1, 5))
                .RuleFor(p => p.AuthorId, f => f.Random.Int(1, 30))
                .RuleFor(p => p.Description, f => f.Lorem.Sentence(random.Next(5, 15)))
                .RuleFor(p => p.CreatedAt, f => f.Date.Between(new DateTime(2017, 1, 1), new DateTime(2020, 1, 1)))
                .RuleFor(p => p.Deadline, f => f.Date.Between(new DateTime(2020, 1, 1), new DateTime(2022, 1, 1)));

            Entities = Enumerable.Range(1, 10)
                .Select(e => fake.Generate());
        }
    }
}
