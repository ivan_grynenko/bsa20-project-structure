﻿using Bogus;
using DAL.Base;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Linq;

namespace DAL.Repositories
{
    public class TeamRepository : Repository<Team>, ITeamRepository
    {
        public TeamRepository()
        {
            FakeData();
        }

        private void FakeData()
        {
            var teams = new string[] { "Team A", "Team B", "Team C", "Team D", "Team E" };
            var id = 1;
            var fake = new Faker<Team>()
                .RuleFor(t => t.Id, f => id++)
                .RuleFor(t => t.Name, f => teams[id - 2])
                .RuleFor(t => t.CreatedAt, f => f.Date.Between(new DateTime(2015, 1, 1), DateTime.Now));

            Entities = Enumerable.Range(1, 5)
                .Select(e => fake.Generate());
        }
    }
}
