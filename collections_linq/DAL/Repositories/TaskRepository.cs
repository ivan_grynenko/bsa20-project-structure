﻿using Bogus;
using DAL.Base;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Linq;

namespace DAL.Repositories
{
    public class TaskRepository : Repository<Tasks>, ITaskRepository
    {
        public TaskRepository()
        {
            FakeData();
        }

        private void FakeData()
        {
            var rendom = new Random();
            var id = 1;
            var fake = new Faker<Tasks>()
                .RuleFor(t => t.Id, f => id++)
                .RuleFor(t => t.Name, f => f.Random.Word())
                .RuleFor(t => t.PerformerId, f => f.Random.Int(1, 30))
                .RuleFor(t => t.ProjectId, f => f.Random.Int(1, 10))
                .RuleFor(t => t.State, f => f.PickRandom<TaskStates>())
                .RuleFor(t => t.Description, f => f.Lorem.Sentence(rendom.Next(5, 15)))
                .RuleFor(t => t.CreatedAt, f => f.Date.Between(new DateTime(2017, 1, 1), new DateTime(2018, 12, 12)))
                .RuleFor(t => t.FinishedAt, f => f.Date.Between(new DateTime(2017, 1, 1), DateTime.Now));

            Entities = Enumerable.Range(1, 50)
                .Select(e => fake.Generate());
        }
    }
}
