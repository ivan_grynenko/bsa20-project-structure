﻿using DAL.Models;
using DAL.Repositories;

namespace DAL.Base
{
    public class UnitOfWork : IUnitOfWork
    {
        private Repository<Project> projectRepository;
        private Repository<Tasks> taskRepository;
        private Repository<Team> teamRepository;
        private Repository<User> userRepository;

        public IRepository<Project> ProjectRepository => projectRepository ?? (projectRepository = new ProjectRepository());
        public IRepository<Tasks> TaskRepository => taskRepository ?? (taskRepository = new TaskRepository());
        public IRepository<Team> TeamRepository => teamRepository ?? (teamRepository = new TeamRepository());
        public IRepository<User> UserRepository => userRepository ?? (userRepository = new UserRepository());

        public void SaveChanges()
        {
            //saving ...
        }
    }
}
