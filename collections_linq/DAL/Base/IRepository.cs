﻿using DAL.Models;
using System;
using System.Collections.Generic;

namespace DAL.Base
{
    public interface IRepository<TEntity> : IDisposable
        where TEntity : BaseModel
    {
        IEnumerable<TEntity> GetAll();
        TEntity GetById(int id);
        void Add(IEnumerable<TEntity> entities);
        void Add(TEntity entity);
        void Remove(IEnumerable<TEntity> entities);
        void Remove(TEntity entity);
        void Update(IEnumerable<TEntity> entities);
        void Update(TEntity entity);
    }
}
