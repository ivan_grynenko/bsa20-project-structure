﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Base
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : BaseModel
    {
        private IEnumerable<TEntity> entities;

        public virtual IEnumerable<TEntity> Entities
        {
            get => entities;
            private protected set => entities = value;
        }

        public void Add(IEnumerable<TEntity> entities)
        {
            var temp = this.entities.ToList();
            temp.AddRange(entities);
            this.entities = temp;
        }

        public void Add(TEntity entity)
        {
            var temp = entities.ToList();
            temp.Add(entity);
            entities = temp;
        }

        public IEnumerable<TEntity> GetAll()
        {
            return entities;
        }

        public TEntity GetById(int id)
        {
            return entities.FirstOrDefault(e => e.Id == id);
        }

        public void Remove(IEnumerable<TEntity> entities)
        {
            var temp = this.entities.Except(entities);
            this.entities = temp;
        }

        public void Remove(TEntity entity)
        {
            var temp = entities.ToList();
            temp.Remove(entity);
            this.entities = temp;
        }

        public void Update(IEnumerable<TEntity> entities)
        {
            var temp = entities.ToList();

            foreach (var entity in entities)
            {
                var item = this.entities.FirstOrDefault(e => e.Id == entity.Id);

                if (item == null) throw new ArgumentException("Item wasn't found in the row");

                var index = temp.IndexOf(item);
                temp[index] = entity;
            }

            entities = temp;
        }

        public void Update(TEntity entity)
        {
            var temp = entities.ToList();
            var item = temp.FirstOrDefault(e => e.Id == entity.Id);

            if (item != null)
            {
                var index = temp.IndexOf(item);
                temp[index] = entity;
                entities = temp;
            }
            else
                throw new ArgumentException("Item wasn't found");
        }

        public void Dispose()
        {
            entities = null;
        }
    }
}
