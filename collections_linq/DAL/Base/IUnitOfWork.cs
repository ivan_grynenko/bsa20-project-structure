﻿using DAL.Models;

namespace DAL.Base
{
    public interface IUnitOfWork
    {
        IRepository<Project> ProjectRepository { get; }
        IRepository<Tasks> TaskRepository { get; }
        IRepository<Team> TeamRepository { get; }
        IRepository<User> UserRepository { get; }

        void SaveChanges();
    }
}